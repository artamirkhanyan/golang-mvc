# simple go-MVC-API #

**version**: 0.001
## Status

Very experimental.

### Installation

Install the dependencies and start the server.

```sh
$ go get github.com/go-sql-driver/mysql
$ go get github.com/gorilla/mux
```

### Run server

```sh
$ go run api/server.go
```

### Defult port :3080

url: http://localhost:3080

The port can be changed in config/config.go
