package routes

import (
	"net/http"

	"../controllers"
)

type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
	Middleware  string
}

type Routes []Route

var RoutesCollection = Routes{
	Route{
		"Index",
		"GET",
		"/",
		controllers.Index,
		"noauth",
	},
	Route{
		"TestGet",
		"GET",
		"/testget",
		controllers.TestGet,
		"auth",
	},
}
