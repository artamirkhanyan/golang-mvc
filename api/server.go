package main

import (
	"log"
	"net/http"

	"../api/config"
	"../api/system"
	_ "github.com/go-sql-driver/mysql"
)

func main() {
	router := system.NewRouter()
	log.Fatal(http.ListenAndServe(":"+config.Server["port"], router))
}
