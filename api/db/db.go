package db

import (
	"database/sql"
	"fmt"

	"../config"
)

func DbConnect() *sql.DB {
	db, err := sql.Open("mysql", config.Db["user"]+":"+config.Db["pass"]+"@tcp("+config.Db["host"]+":"+config.Db["port"]+")/"+config.Db["name"])
	if err != nil {
		fmt.Print(err.Error())
	}

	// make sure connection is available
	err = db.Ping()
	if err != nil {
		fmt.Print(err.Error())
	}

	return db
}

func Select(sqlString string) (interface{}, error) {

	db := DbConnect()
	rows, err := db.Query(sqlString)
	if err != nil {
		return "", err
	}
	defer rows.Close()
	columns, err := rows.Columns()
	if err != nil {
		return "", err
	}
	count := len(columns)
	tableData := make([]map[string]interface{}, 0)
	values := make([]interface{}, count)
	valuePtrs := make([]interface{}, count)
	for rows.Next() {
		for i := 0; i < count; i++ {
			valuePtrs[i] = &values[i]
		}
		rows.Scan(valuePtrs...)
		entry := make(map[string]interface{})
		for i, col := range columns {
			var v interface{}
			val := values[i]
			b, ok := val.([]byte)
			if ok {
				v = string(b)
			} else {
				v = val
			}
			entry[col] = v
		}
		tableData = append(tableData, entry)
	}
	defer db.Close()

	return tableData, nil

}
