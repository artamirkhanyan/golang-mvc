package system

import (
	"errors"
	"net/http"
	"strings"

	"../db"
	"../libs"
	"../routes"
	"github.com/gorilla/mux"
)

func NewRouter() *mux.Router {

	router := mux.NewRouter().StrictSlash(true)

	for _, route := range routes.RoutesCollection {
		var handler http.Handler

		handler = route.HandlerFunc
		handler = logger(handler, route.Name)

		router.
			Methods(route.Method).
			Path(route.Pattern).
			Name(route.Name).
			Handler(middleware(handler, route.Middleware))
	}

	return router
}

func middleware(h http.Handler, auth string) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if auth == "auth" {
			apiKey := r.Header.Get("Apikey")
			authErr := authenticate(apiKey)
			if authErr != nil {
				resp := map[string]string{
					"status":  "error",
					"message": authErr.Error(),
				}
				libs.OptJSON(w, resp)
			} else {
				h.ServeHTTP(w, r)
			}
		} else if auth == "noauth" {
			h.ServeHTTP(w, r)
		}
	})
}

func authenticate(key string) error {

	if key == "" {
		return errors.New("Access Denied. API key is misssing")
	}

	var id string
	db := db.DbConnect()
	err := db.QueryRow("select id from api_keys where `key`= ?", key).Scan(&id)
	//TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ
	if err != nil {
		if strings.Contains(err.Error(), "Error") {
			return err
		}
		return errors.New("Access Denied. Invalid API key")
	}

	return nil
}
