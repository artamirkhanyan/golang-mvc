package libs

import (
	"encoding/json"
	"fmt"
	"net/http"
)

func OptJSON(w http.ResponseWriter, data interface{}) {

	jsonData, err := json.Marshal(data)

	if err != nil {
		fmt.Print(err)
	}

	w.Header().Set("Content-Type", "application/json")
	fmt.Fprintf(w, string(jsonData))
}
