package controllers

import (
	"fmt"
	"net/http"

	"../db"
	"../libs"
)

func Index(w http.ResponseWriter, r *http.Request) {

	output := make(map[string]interface{})
	output["status"] = "success"
	output["message"] = "Welcome to goAPI."

	libs.OptJSON(w, output)

}

func TestGet(w http.ResponseWriter, r *http.Request) {
	data, _ := db.Select("select id, email, first_name, last_name from users;")

	//test map on interfase{}
	switch myMap := data.(type) {
	case []map[string]interface{}:

		for _, v := range myMap {
			for a, b := range v {
				fmt.Println(fmt.Sprintf("%s : %s", a, b))
			}
			fmt.Println("----")
		}
	}

	output := make(map[string]interface{})
	output["status"] = "success"
	output["message"] = "endpoint: TESTget."
	output["data"] = data
	libs.OptJSON(w, output)
}
