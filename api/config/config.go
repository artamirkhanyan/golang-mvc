package config

var Db = map[string]string{
	"host": "localhost",
	"port": "3306", //3306 is default
	"user": "root",
	"pass": "",
	"name": "testdb",
}

var Server = map[string]string{
	"port": "3080",
}
